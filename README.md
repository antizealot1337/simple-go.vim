# simple-go.vim

A (hopefully) simpler vim plugin for working with Go files.

## Why not https://github.com/fatih/vim-go?
When I first started using Go I used (like many I assume)
https://github.com/fatih/vim-go.git. It met my needs and was really superb. It
made using Go and Vim a dream. Over time the project continued to gather 
features and dependancies (many in the form of Go binaries). The problem became
that with my setup the plugin stopped functioning as it used to with little
quirks. This seriously impacted my coding as sometimes the package detection
failed to detect properly. I was also fearful of updating the plugin since new
binary dependancies failed to download and install. So I decided to take the
plugin and fix it for myself.
